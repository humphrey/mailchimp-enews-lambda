import base64
import json

import requests


class GitHubUpdater(object):

    def __init__(self, url, username, token):
        self.url = url
        self.auth = (username, token)

    def get_remote_sha(self):
        r = requests.get(self.url, auth=self.auth)
        if r.status_code == 200:
            return r.json().get('sha', '')

    def get_remote_content(self):
        r = requests.get(self.url, auth=self.auth)
        if r.status_code == 200:
            c = r.json().get('content', '')
            if c:
                return base64.b64decode(c)

    def update_remote_if_different(self, html):
        remote_content = self.get_remote_content()
        if remote_content != html:
            print '**Changed, so uploading to GitHub**'
            self.update_remote(html)
            return True
        else:
            print 'No change'
            return False

    def update_remote(self, html, remote_sha=None):
        remote_sha = remote_sha or self.get_remote_sha()
        data = {
            "message": "Latest eNews auto-updated from AWMS Lambda",
            "content": base64.b64encode(html),
            "sha": remote_sha,
        }
        json_data = json.dumps(data)
        r = requests.put(self.url, auth=self.auth, data=json_data)
        if not (200 <= r.status_code < 300):
            raise Exception(u'Was not able to update file on Github!!!\n{}\n{}'.format(str(r.status_code), r.content).decode('utf-8'))

    def create_remote_file(self, content):
        data = {
            "message": "Latest eNews auto-updated from AWMS Lambda",
            "content": base64.b64encode(content),
        }
        json_data = json.dumps(data)
        r = requests.put(self.url, auth=self.auth, data=json_data)
        if not (200 <= r.status_code < 300):
            raise Exception(u'Was not able to create file on Github!!!\n{}\n{}'.format(str(r.status_code), r.content).decode('utf-8'))
