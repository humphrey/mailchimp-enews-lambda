import json
import pytz
import requests
from datetime import datetime

from mailchimpfeed.feed import Feed
from mailchimpfeed.github import GitHubUpdater
from mailchimpfeed.images import ImageResizer


def sync_now(mailchimp_feed_url, soundcloud_feed_url, github_username, github_token, github_api_url_html, github_api_url_js, github_api_url_data, github_api_url_images, github_api_url_podcast, media_url):

    # Fetch the MailChimp HTML
    f = Feed(feed_url=mailchimp_feed_url)
    html, link = f.fetch_latest_pruned_html()

    # Update the raw HTML to Github if it's changed
    html_updater = GitHubUpdater(url=github_api_url_html, username=github_username, token=github_token)
    r1 = html_updater.update_remote_if_different(html)


    if r1:

        resizer = ImageResizer(media_url, github_username, github_token, github_api_url_images)
        html2 = resizer.post_process_images(html)

        js = u'document.write(%s);' % json.dumps(html2)

        # Update the raw JS to Github if it's changed
        js_updater = GitHubUpdater(url=github_api_url_js, username=github_username, token=github_token)
        r2 = js_updater.update_remote_if_different(js)

        # Update the data to Github if it's changed
        data_updater = GitHubUpdater(url=github_api_url_data, username=github_username, token=github_token)
        r3 = data_updater.update_remote_if_different(json.dumps({
            'html': html2,
            'link': link,
            'date': datetime.now(pytz.timezone('Australia/Hobart')).strftime('%Y-%m-%dT%H:%M:%S%z'),
        }, indent=4))

    else:
        r2 = False

    r_soundcloud = requests.get(soundcloud_feed_url)
    if 200 >= r_soundcloud.status_code < 300:

        # Update the raw HTML to Github if it's changed
        soundcloud_updater = GitHubUpdater(url=github_api_url_podcast, username=github_username, token=github_token)
        soundcloud_updater.update_remote_if_different(json.dumps(json.loads(r_soundcloud.content), indent=2))

    return r1 or r2
