import re
import os
import hashlib
import requests
from datetime import datetime
from bs4 import BeautifulSoup
from PIL import Image
from PIL.ImageCms import profileToProfile
from cStringIO import StringIO

from mailchimpfeed.github import GitHubUpdater


class ImageResizer(object):

    quality = 85
    optimize = 1

    def __init__(self, media_url, github_username, github_token, github_api_images):
        self.media_url = media_url # + datetime.today().strftime('%Y/')
        self.github_username = github_username
        self.github_token = github_token
        self.github_api_images = github_api_images

    def post_process_images(self, html):

        soup = BeautifulSoup(html, 'html.parser')

        for img in soup.find_all('img'):
            if 'src' in img.attrs:
                print
                print 'Considering image', img['src']
                src = img['src']
                ext = os.path.splitext(src)[1].lower()
                if ext and ext in ['.jpg', '.jpeg', '.png', '.tiff']:

                    new_file_name = hashlib.sha224(src).hexdigest() + ext
                    print 'New file name:', new_file_name

                    img['data-original-src'] = src
                    img['src'] = u'{}{}'.format(self.media_url, new_file_name)

                    img_updater = GitHubUpdater(
                        url=self.github_api_images + new_file_name,
                        username=self.github_username,
                        token=self.github_token
                    )
                    remote_content = img_updater.get_remote_content()
                    print 'Remote Content?', bool(remote_content), len(remote_content or '')
                    if not remote_content:

                        # Ok, the file doesn't exist, so upload
                        print 'Download mailchimp image...',
                        r = requests.get(src)
                        print r.status_code
                        if 200 <= r.status_code < 300:
                            new_image_contents = self.resize_image(r.content, ext)
                            print 'Uploading image...'
                            img_updater.create_remote_file(new_image_contents)

        return soup.prettify().encode('utf-8')

    def resize_image(self, file_contents, ext):

        im = self.load_image(file_contents)

        w, h = im.size

        if w > 700:
            im.thumbnail((600, 1200), Image.ANTIALIAS)

        return self.save_image_to_memory(im, ext)

    def load_image(self, file_contents):

        f1 = StringIO()
        f1.write(file_contents)
        f1.seek(0)

        im = Image.open(f1, mode='r')

        if im.mode == 'CMYK' and im.format == "JPEG":
            im = self.convert_cmyk_to_rgb(im)

        return im

    def save_image_to_memory(self, im, ext):

        # Get transparency info
        transparency = None
        if "transparency" in im.info:
            transparency = im.info["transparency"]

        fmt = None
        if ext.lower() == ".jpg" or ext.lower() == ".jpeg":
            fmt = "JPEG"
        elif ext == ".png":
            fmt = "PNG"
        elif ext == ".gif":
            fmt = "GIF"
        elif ext == ".tif" or ext == ".tiff":
            fmt = "TIFF"

        f = StringIO()
        if not transparency is None:
            im.save(f, format=fmt, quality=self.quality, optimize=self.optimize, transparency=transparency)
        else:
            im.save(f, format=fmt, quality=self.quality, optimize=self.optimize)

        f.seek(0)
        return f.read()

    def convert_cmyk_to_rgb(self, im):
        if im.mode == 'CMYK':
            CMYK_PROFILE = 'colour_profiles/CMYK-USWebCoatedSWOP.icc'
            sRBG_PROFILE = 'colour_profiles/sRGB_v4_ICC_preference.icc'
            print 'CMYK conversion START ', CMYK_PROFILE, sRBG_PROFILE
            im = profileToProfile(im, CMYK_PROFILE, sRBG_PROFILE, outputMode='RGB')
            print 'CMYK conversion FINISH'
        return im