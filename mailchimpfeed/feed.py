import feedparser
from bs4 import BeautifulSoup


class Feed(object):

    def __init__(self, feed_url):
        self.feed_url = feed_url

    def fetch_latest_html(self):
        d = feedparser.parse(self.feed_url)
        if len(d.entries):
            html = d.entries[0].description #.encode('utf-8')
        else:
            html = ''
        return html, d.entries[0].link

    def process_html(self, html):
        soup = BeautifulSoup(html, 'html.parser')
        soup = self.prune_unwanted_tags(soup)
        body_table = soup.select('#bodyTable')
        if body_table:
            soup = body_table[0]
        return soup.prettify().encode('utf-8')

    def prune_unwanted_tags(self, soup):

        # Strip the annoying header and footer blocks
        selectors_to_strip = [
            '#templatePreheader',
            'table.mcnShareBlock',
            'table.mcnFollowBlock',
            'a.utilityLink',
            '.footerContainer',
        ]
        for selector in selectors_to_strip:
            for e in soup.select(selector):
                e.replaceWith('')

        # REMOVE THE MAIL CHIMP MONKEY
        for e in soup.findAll("img", {"alt" : "Email Marketing Powered by MailChimp"}):
            for e2 in e.parent():
                e2.replaceWith('')

        return soup

    def trim_to_element(self, soup, selector):
        return soup.select(selector)

    def fetch_latest_pruned_html(self):
        html, link = self.fetch_latest_html()
        html = self.process_html(html)
        return html, link
