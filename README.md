# MailChimp Latest Campaign AWS Lambda Function

A simple AWMS Lambda function that 

 - fetches a feed from MailChimp, 
 - extracts the HTML from the latest newsletter
 - and saves a safe html version in a github repository, so that it can be accessed later by a static site generator.

## Getting Started with the pre-zipped code package

1. Download [Lambda-Archive.zip](https://github.com/freelifechurch/mailchimp-enews-lambda/blob/master/Lambda-Archive.zip)

## Getting Started with the code

1. Download and extract this repository

    `$ git clone https://github.com/freelifechurch/mailchimp-enews-lambda.git`
    
    `$ cd mailchimp-enews-lambda`

2. Use pip to install dependancies into project folder (-t specifies which directory to install into)

    `$ pip install -r requirements.txt -t .`

3. Create a zip file of all the files in the project folder.  It's important to not compress the folder, but just the files in the folder, so that Lambda can access the files.  Include all the files and subfolders that pip added.

## Setting up AWS Lambda

Upload the zip to AWS and configure.  Make sure you specify the following enviroment variants in the console. EG.

    mailchimp_feed_url = "http://us8.campaign-archive2.com/feed?u=000000000002&id=111111111111"
    github_api_url_html = "https://api.github.com/repos/yourname/yourrepo/contents/latest-enews.html"
    github_api_url_js = "https://api.github.com/repos/yourname/yourrepo/contents/latest-enews.js"
    github_username = "fred"
    github_token = "1111122222233333344444555556666677777888889999900000"
