import os

from mailchimpfeed.sync import sync_now


def lambda_handler(*args, **kwargs):

    return sync_now(
        mailchimp_feed_url = os.environ.get('mailchimp_feed_url', ''),
        soundcloud_feed_url = os.environ.get('soundcloud_feed_url', ''),
        github_api_url_html = os.environ.get('github_api_url_html', ''),
        github_api_url_js = os.environ.get('github_api_url_js', ''),
        github_api_url_data = os.environ.get('github_api_url_data', ''),
        github_api_url_images = os.environ.get('github_api_url_images', ''),
        github_api_url_podcast = os.environ.get('github_api_url_podcast', ''),
        github_username = os.environ.get('github_username', ''),
        github_token = os.environ.get('github_token', ''),
        media_url = os.environ.get('media_url', '/'),
    )
